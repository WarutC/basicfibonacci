def fibonacci_sequence(start_index, stop_index):
    counts = range(start_index, stop_index + 1)
    current = start_index
    lastnum = 0

    for count in counts:
        if count < 10:
            print(str(count) + "    :    " + str(current))
        else:
            print(str(count) + "   :    " + str(current))

        givelast = current
        current = current + lastnum
        lastnum = givelast


def main():
    start_index = 1
    stop_index = 21

    # Result
    # 0, 1, 1, 2, 3, 5, 8, 13, 21, 34
    print('0, 1, 1, 2, 3, 5, 8, 13, 21, 34')

    print("n    :    Fibo  :")
    fibonacci_sequence(start_index, stop_index)


if __name__ == "__main__":
    main()
